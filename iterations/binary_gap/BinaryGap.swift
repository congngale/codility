//binary gap solution
public func solution(N : Int) -> Int {
    // write your code in Swift 2.2 (Linux)
    var rs:Int = 0
    var current:Int = -1
    var number:Int = N
    
    for _ in 1...32 {
        if((number%2) != 0) {
            if(current > rs) {
                rs = current
            }
            current = 0
        } else {
            if(current != -1) {
                current += 1
            }
        }
        
        //update number
        number = number/2
    }
    return  rs
}

//input from user
var args = [String]()
for arg in Process.arguments {
    args.append(arg)
}

//main application
if(args.count == 2) {
    var number:Int? = Int(args[1])
    if (number != nil) {
        print("Your input number = \(number!)")
        print(solution(number!))
    } else {
        print("Please input a number instead of string")
    }
} else {
    print("Please input your number with format: swift BinaryGap.swift #your_number")
}
