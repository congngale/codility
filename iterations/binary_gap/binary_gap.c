#include <stdio.h>
#include <stdlib.h>

int solution(int N) {
	// write your code in C99 (gcc 4.8.2)
	int i;
	int rs = 0;
	int current = -1;

	for (i=(sizeof(int)*8);i>0;i--) {
		if (N % 2) {
			if(current == -1){
				current = 0;
			} else {
				if(current > rs){
					rs = current;
				}
				current = 0;
			}
		} else {
			if (current != -1) {
				current++;
			}
		}

		//update N
		N = N/2;
	}

	return rs;
}

int main(int argc, char *agrv[]){
	if(argc == 2) {
		int input = atoi(agrv[1]);
		printf("Your input  number = %d\n",input);
		printf("result = %d\n",solution(input));
	} else {
		printf("Please input your number with format: ./binary_gap #your_number\n");
	}
	return 0;
}
