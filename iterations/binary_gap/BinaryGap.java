import java.lang.Integer;

class BinaryGap {

	public BinaryGap(){
	}

	public static int solution(int N) {
		// write your code in Java SE 8
		int rs = 0;
		int current = -1;
		int mask = 1 << 31;

		//assum that int has 4 bytes
		for(int i = 0; i < 32; i++) {
			if((mask & N) != 0){
				if(current == -1){
					current = 0;
				} else {
					if(current > rs){
						rs = current;
					}
					current = 0;
				}
			} else {
				if(current != -1) {
					current++;
				}
			}

			//update mask
			mask = mask >>> 1;
		}

		return rs;
	}

	public static void main(String[] args){
		if(args.length == 1){
			try {
				int input = Integer.parseInt(args[0]);
				System.out.println("Your input = "+input);
				System.out.println("result = "+solution(input));			
			} catch (NumberFormatException e){
				System.out.println("Please check your input, it's not a number");
			}
		} else {
			System.out.println("Please input your number with format: java BinaryGap #your_number");
		}
	}
}
