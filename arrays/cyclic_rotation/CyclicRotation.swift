public func solution(inout A : [Int], _ K : Int) -> [Int] {
    // write your code in Swift 2.2 (Linux)
    let N:Int = A.count
    var rs = [Int]()
    var rotate:Int = 0
    var pos:Int = 0


    if(N != 0) {
        rotate = K % N

        if (rotate != 0) {
            rs = A;
            for i in 0..<N {
                pos = i + rotate

                if(pos >= N) {
                    pos = pos - N
                }

                A[pos] = rs[i];
            }

        }
    }
    return A;
}

//example data input
var A: [Int] = [1, 2, 3, 5, 8]
var K:Int = 4

print("Example input = \(A)")
print("N = \(A.count)")
print("K = \(K)")
print("")
print("result = \(solution(&A,K))")
