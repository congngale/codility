class CyclicRotation {
    public static int[] solution(int[] A, int K) {
        // write your code in Java SE 8
        int N = A.length;
        int rs[] = new int[N];
        int pos, rotate;

        if (N != 0) {
            rotate = K % N;

            if (rotate  != 0) {
                for (int i=0;i<N;i++) {
                    pos = i + rotate;

                    if (pos < N) {
                        rs[pos] = A[i];
                    } else {
                        rs[pos - N] = A[i];
                    }
                }   
            }
        }
        return rs;
    }

    public static void main(String args[]){
        //example input
        int A[] = {2,3,5,6,7};
        int K = 4;

        //print input
        System.out.print("input array = [");
        for(int i=0;i<A.length;i++) {
            if(i != (A.length - 1)) {
                System.out.print(A[i]+",");
            } else {
                System.out.println(A[i]+"]");
            }
        }
        System.out.println("input N = "+A.length);
        System.out.println("input K = "+K);
        System.out.println("");

        int rs[] = solution(A,K); 
        System.out.print("result array = [");
        for(int i=0;i<rs.length;i++) {
            if(i != (rs.length - 1)) {
                System.out.print(rs[i]+",");
            } else {
                System.out.println(rs[i]+"]");
            }
        }
    }
}
