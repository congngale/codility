#include <stdio.h>
#include <stdlib.h>

struct Results {
    int *A;
    int N;
};

struct Results solution(int A[], int N, int K) {
    struct Results result;
    // write your code in C99 (gcc 4.8.2)
    int i, pos, rotate;
    int *rs = malloc(N * sizeof(int));

    //init struct
    result.A = malloc(N * sizeof(int));

    //default result
    result.A = A;
    result.N = N;

    if(N != 0) {
        rotate = K % N;
        if(rotate != 0) {
            for (i=0;i<N;i++) {
                pos = i + rotate;
                if(pos < N) {
                    rs[pos] = A[i];
                } else {
                    rs[(pos-N)] = A[i];
                }
            }

            //update result
            result.A = rs;
        }
    }

    return result;
}

int main(int agrc, char ** agrs){
    //init variable to run
    int i;

    //example input data
    int A[5] = {1,2,4,5,6};
    int N = 5;
    int K = 4;

    //print example input
    printf("input array = [");
    for(i=0;i<N;i++) {
        if(i != (N-1)) {
            printf("%d,",A[i]);
        } else {
            printf("%d",A[i]);
        }
    }
    printf("]\n");
    printf("input N = %d\n",N);
    printf("input K = %d\n",K);

    //execute solution
    struct Results rs = solution(A, N, K);

    //print result
    printf("\nresult = [");
    for(i=0;i<rs.N;i++){
        if(i != (rs.N-1)) {
            printf("%d,",rs.A[i]);
        } else {
            printf("%d",rs.A[i]);
        }
    }
    printf("]\n");
}
