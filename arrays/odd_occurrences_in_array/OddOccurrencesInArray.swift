public func solution(inout A : [Int]) -> Int {
    // write your code in Swift 2.2 (Linux)
    var rs:Int = 0
        
    for i in 0..<A.count {
        rs ^= A[i]
    }
                
    return rs
}

//example data input
var A: [Int] = [1, 2, 1, 2, 3]

print("Example input = \(A)")
print("N = \(A.count)")
print("")
print("result = \(solution(&A))")
