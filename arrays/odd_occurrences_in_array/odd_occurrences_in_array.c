#include <stdio.h>

int solution(int A[], int N) {
    // write your code in C99 (gcc 4.8.2)
    int i;
    int rs = 0;

    for(i=0;i<N;i++){
        rs ^= A[i];
    }

    return rs;
}

int main(int argc, char **args) {
    //example intput 
    int i;
    int A[5] = {1,2,1,2,3};
    int N = 5;

    //print example input
    printf("input array = [");
    for(i=0;i<N;i++) {
        if(i != (N-1)) {
            printf("%d,",A[i]);
        } else {
            printf("%d",A[i]);
        }
    }
    printf("]\n");
    printf("input N = %d\n",N);
    printf("\n");

    //result
    int rs = solution(A,N);
    printf("result = %d\n",rs);
}
