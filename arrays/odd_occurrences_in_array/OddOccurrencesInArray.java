class OddOccurrencesInArray {

    public static int solution(int[] A) {
        // write your code in Java SE 8
        int rs = 0;

        for(int i=0;i<A.length;i++) {
            rs ^= A[i];
        }

        return rs;
    }

    public static void main(String args[]){
        //example input
        int A[] = {1,2,1,2,3};

        //print input
        System.out.print("input array = [");
        for(int i=0;i<A.length;i++) {
            if(i != (A.length - 1)) {
                System.out.print(A[i]+",");
            } else {
                System.out.println(A[i]+"]");
            }
        }
        System.out.println("input N = "+A.length);
        System.out.println("");

        int rs = solution(A);
        System.out.println("result = "+rs);
    }
}
